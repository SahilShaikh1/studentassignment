/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.studentassignmentmanagement.model.master;

import com.mycompany.studentassignmentmanagement.model.BaseEntity.BaseEntity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Sahil
 */
@Entity
@Table(name = "assignment")
public class Assignment extends BaseEntity {

    private String assignmentName;

    private Date assignmentDate;
    private Date dueDate;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "assignment", cascade = CascadeType.ALL, orphanRemoval = true)
     
    private List<StudentAssignment> studentAssignementList = new ArrayList();

    public List<StudentAssignment> getStudentAssignementList() {
        return studentAssignementList;
    }

    public void setStudentAssignementList(List<StudentAssignment> studentAssignementList) {
        this.studentAssignementList = studentAssignementList;
    }
    
    

    public String getAssignmentName() {
        return assignmentName;
    }

    public void setAssignmentName(String assignmentName) {
        this.assignmentName = assignmentName;
    }

    public Date getAssignmentDate() {
        return assignmentDate;
    }

    public void setAssignmentDate(Date assignmentDate) {
        this.assignmentDate = assignmentDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

}
