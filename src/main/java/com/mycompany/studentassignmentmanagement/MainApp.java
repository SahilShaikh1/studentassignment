package com.mycompany.studentassignmentmanagement;

import dataservice.SojSessionFactory;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class MainApp extends Application {

    static BorderPane pane = new BorderPane();

    public static BorderPane getPane() {
        return pane;
    }
    
    
    
    @Override
    public void start(Stage stage) throws Exception {
        SojSessionFactory.configureSessionFactory();
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Login.fxml"));
        
        pane.setCenter(root);
        Scene scene = new Scene(pane);
        scene.getStylesheets().add("/styles/Styles.css");
        
        stage.setTitle("JavaFX and Maven");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
