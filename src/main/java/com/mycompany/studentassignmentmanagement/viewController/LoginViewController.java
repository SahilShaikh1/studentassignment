package com.mycompany.studentassignmentmanagement.viewController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.controllers.LoginController;
import com.mycompany.studentassignmentmanagement.MainApp; 
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Sahil
 */
public class LoginViewController implements Initializable {

    @FXML
    private TextField txtUsername;

    @FXML
    private Button btnLogin;

    @FXML
    private Button btnClear;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private Text txtValidationText;

    /**
     * Initializes the controller class.
     */
    private void setFocusListner(final Control child) {
        child.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if (child instanceof TextField && newValue) {
                    txtValidationText.setText("");
                }

            if (child instanceof Button && newValue) {
                    child.setStyle("-fx-background-color:transparent");
                }
            }
        });

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        setFocusListner(btnLogin);
        setFocusListner(btnClear);
        setFocusListner(txtPassword);
        setFocusListner(txtPassword);

        btnClear.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                clear();
            }
        });

        btnLogin.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                submit();
            }
        });
 
    } 

    private void submit() {
        String userName = txtUsername.getText();
        String password = txtPassword.getText();
        LoginController ctrl = new LoginController();
        if (ctrl.validateLogin(userName, password)) {  
            try {
 
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainWindow.fxml"));
                Parent parent = (Parent)loader.load();
                MainApp.getPane().setCenter(parent);  
            } catch (Exception e) {
                System.out.println(e); 
            }
        }
    }

    private void clear() {
        txtPassword.setText("");
        txtUsername.setText("");

    }

}
