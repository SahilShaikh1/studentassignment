package com.mycompany.studentassignmentmanagement.viewController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.controllers.GlobalController;
import com.mycompany.studentassignmentmanagement.model.master.Assignment;
import com.mycompany.studentassignmentmanagement.utility.Helper.Helper;
import com.mycompany.studentassignmentmanagement.utility.services.WindowService;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Stream;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author user
 */
public class AssignmentViewController implements Initializable {

    @FXML
    private TextField txtAssignmentName, txtAssignmentDate, txtDueDate;

    @FXML
    private TableColumn<Assignment, String> colName, colDate, colDueDate;

    @FXML
    private TableView<Assignment> tableAssignemt;

    @FXML
    private Button btnSave, btnClear;

    private ObservableList<Assignment> assignmentList = FXCollections.observableArrayList();

    private Assignment entity;
    private GlobalController controller;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        btnClear.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                clear();
            }
        });
        btnSave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                save();
            }
        });

        setTableAndColumn();
        entity = new Assignment();
        controller = new GlobalController();
        loadList();
    }

    private void setTableAndColumn() {
        colName.setCellValueFactory(new PropertyValueFactory("assignmentName"));
        colDate.setCellValueFactory(new PropertyValueFactory("assignmentDate"));
        colDueDate.setCellValueFactory(new PropertyValueFactory("dueDate"));
        tableAssignemt.setItems(assignmentList);
        tableAssignemt.getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<Assignment>() {
                    @Override
                    public void changed(ObservableValue<? extends Assignment> observable, Assignment oldValue, Assignment newValue) {
                        entity = tableAssignemt.getSelectionModel().getSelectedItem();
                        viewDetail();
                    }
                });
    }

    private void loadList() {
        List<Assignment> assignments = controller.getAll(Assignment.class);
        assignmentList.addAll(assignments); 
    }

    private void viewDetail() {
        if (entity != null) {
            txtAssignmentName.setText(entity.getAssignmentName());
            txtAssignmentDate.setText(Helper.convertToString(entity.getAssignmentDate()));
            txtDueDate.setText(Helper.convertToString(entity.getDueDate()));
        }
    }

    private void save() {
        entity.setAssignmentName(txtAssignmentName.getText().toString());
        entity.setAssignmentDate(Helper.convertToDate(txtAssignmentDate.getText().toString()));
        entity.setDueDate(Helper.convertToDate(txtDueDate.getText().toString()));
        
        
        controller.save(entity);
        WindowService.saveMessage(entity,entity, assignmentList);
        
         
        if(assignmentList.contains(entity)){
            assignmentList.set(assignmentList.indexOf(entity), entity);
        }else{
            assignmentList.add(entity);
        } 
        clear();

    }

    private void clear() {
        Stream.of(txtAssignmentName, txtAssignmentDate, txtDueDate)
                .forEach(t -> t.setText(""));
 
        entity = new Assignment();
    }

}
