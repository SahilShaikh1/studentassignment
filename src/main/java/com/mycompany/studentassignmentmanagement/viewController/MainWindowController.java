package com.mycompany.studentassignmentmanagement.viewController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mycompany.studentassignmentmanagement.utility.Constants;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;


/**
 * FXML Controller class
 *
 * @author Tabassum Naaz
 */
public class MainWindowController implements Initializable {

    @FXML
    private Button btnStudent;

    @FXML
    private Button btnAssignment;

    @FXML
    private Button btnStudentAssignment;

    @FXML
    private Button btnUserManagement;

    @FXML
    private BorderPane borderPane;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        btnStudent.setText(Constants.STUDENTS);
        btnAssignment.setText(Constants.ASSIGNMENT);
        btnStudentAssignment.setText(Constants.STUDENTASSIGNMENT);
        btnUserManagement.setText(Constants.USERMANAGEMENT);

        // TODO
        btnStudent.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    loadView(btnStudent.getText());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        btnAssignment.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    loadView(btnAssignment.getText());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        btnStudentAssignment.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    loadView(btnStudentAssignment.getText());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        btnUserManagement.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    loadView(btnUserManagement.getText());
                } catch (IOException ex) {
                    Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        //btnUserManagement.setOnAction(e-> loadView(btnUserManagement.getText()));

    }

    public void loadView(String viewname) throws IOException {


        if (viewname.equals(Constants.STUDENTS)) { 
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/StudentForm.fxml"));
            Parent layout = (Parent) loader.load();
            borderPane.setCenter(layout);

        }
        if (viewname.equals(Constants.ASSIGNMENT)) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Assignment.fxml"));
            Parent layout = (Parent) loader.load();
            borderPane.setCenter(layout);

        }
        if (viewname.equals(Constants.STUDENTASSIGNMENT)) {

        }
        if (viewname.equals(Constants.USERMANAGEMENT)) {

        }

    }

}
