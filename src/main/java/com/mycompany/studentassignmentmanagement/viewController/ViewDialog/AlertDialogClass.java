/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.studentassignmentmanagement.viewController.ViewDialog;

import com.mycompany.studentassignmentmanagement.utility.services.WindowService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author user
 */
public class AlertDialogClass extends VBox {

        @FXML
        private VBox mainDialog;

        @FXML
        private Label lblTitle;

        @FXML
        private Text lblContentMsg;

        @FXML
        private Button btnClose, btnOk;

        public Parent parent;
        private Boolean isConfirmBtnPressed = false;

        public AlertDialogClass(Boolean isError, String content, boolean isConfimDialog) {
                try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AlertDialog.fxml"));
                        loader.setRoot(this);
                        loader.setController(this);
                        parent = loader.load();
                        lblContentMsg.setText(content);
                        if (isError) {
                                lblContentMsg.setFill(Color.RED);
                        }
                        btnClose.setOnAction(e -> stage.close());
                        btnClose.requestFocus();
                        WindowService.ShortCutKey.addEventhandlerOnBtn(KeyCode.ENTER, btnClose);
                        if (isConfimDialog) {
                                btnOk.setVisible(true);
                                btnClose.setText("Back");
                                WindowService.ShortCutKey.addEventhandlerOnBtn(KeyCode.ENTER, btnOk);
                                btnOk.setOnAction(e -> {
                                        isConfirmBtnPressed = true;
                                        stage.close();
                                });
                        }
                        createStage();
                } catch (Exception ex) {
                        System.out.println(ex);
                }
        }

        Stage stage;

        private void createStage() {
                stage = new Stage();
                Scene scene = new Scene(parent);
                stage.setScene(scene);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initStyle(StageStyle.UNDECORATED);
                stage.showAndWait();
        }

        public boolean getValue() {
                return isConfirmBtnPressed;
        }
}
