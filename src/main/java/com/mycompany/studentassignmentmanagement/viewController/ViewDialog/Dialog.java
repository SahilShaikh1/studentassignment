/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.studentassignmentmanagement.viewController.ViewDialog;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author user
 */
public class Dialog {

        private final Parent mainNode;
        private Stage stage;

        private Boolean isShowUndecorated = false;

        public Dialog(Parent mainNode, String title, Stage ownerStage) {
                this.mainNode = mainNode;
                initialize(title, ownerStage);
        }

        public Dialog(Parent mainNode, String title, Stage ownerStage, boolean isUndecorated) {
                this.mainNode = mainNode;
                this.isShowUndecorated = isUndecorated;
                initialize(title, ownerStage);

        }

        private void initialize(String title, Stage ownerStage) {
                stage = new Stage();
                stage.setTitle(title);

                stage.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
                        if (KeyCode.ESCAPE == event.getCode()) {
                                stage.close();
                        }
                });

                if (ownerStage != null) {
                        stage.initOwner(ownerStage);
                }

                if (isShowUndecorated) {
                        stage.initStyle(StageStyle.UNDECORATED);
                }

                stage.initModality(Modality.APPLICATION_MODAL);
                Scene scene = new Scene(this.mainNode);

                stage.setScene(scene);
                stage.show();

        }

        public void closeDialog() {
                if (stage != null) {
                        stage.close();
                }
        }

        public Stage getStage() {
                return stage;
        }

}
