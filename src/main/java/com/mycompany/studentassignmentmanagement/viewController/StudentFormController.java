/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.studentassignmentmanagement.viewController;

import com.controllers.LoginController;
import com.mycompany.studentassignmentmanagement.Helper.Helper;
import com.mycompany.studentassignmentmanagement.model.master.Assignment;
import com.mycompany.studentassignmentmanagement.model.master.Students;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javax.swing.text.StyledEditorKit;

/**
 *
 * @author Sahil
 */
public class StudentFormController implements Initializable {

    private Students studentEntity;

    private LoginController controller;

    @FXML
    private TextField txtGuardianPhoneNumber, txtStudentPhoneNumber, txtName, txtAddress, txtGuardianName;

    @FXML
    private DatePicker dpDOB;

    @FXML
    private Text txtValidationText;

    @FXML
    private Button btnSave, btnClear;

    @FXML
    private TableColumn<Students, String> colName;
    @FXML
    private TableColumn<Students, String> colPhoneNumber;

    @FXML
    private TableView<Students> tableStudent;

    private ObservableList<Students> list = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btnClear.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                clear();
            }
        });
        Helper.numberValidation(txtStudentPhoneNumber);
        btnSave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                save();
            }
        });
        setTableAndColumn();
        controller = new LoginController();
        studentEntity = new Students();
        loadList();
    }

    private void save(){
             if (isValidate()==true){
              
        studentEntity.setGuardianPhoneNo(txtGuardianPhoneNumber.getText());
        studentEntity.setStudentPhoneNo(txtStudentPhoneNumber.getText());
        studentEntity.setName(txtName.getText());
        studentEntity.setDateOfBirth(Helper.getDatefromLocalDate(dpDOB.getValue()));
        studentEntity.setAddress(txtAddress.getText());
        studentEntity.setGuardianName(txtGuardianName.getText());
        controller.save(studentEntity);
             }
        {
        if(list.contains(studentEntity)){
            list.set(list.indexOf(studentEntity), studentEntity);
        }else{
            list.add(studentEntity);
        } 
        }
        this.clear();
    }

    private void clear() {
        txtGuardianPhoneNumber.setText("");
        txtStudentPhoneNumber.setText("");
        txtName.setText("");
        txtAddress.setText("");
        txtGuardianName.setText("");
    
        studentEntity = new Students();

    }

    private void setTableAndColumn() {
            colName.setCellValueFactory(new PropertyValueFactory("name"));
        colPhoneNumber.setCellValueFactory(new PropertyValueFactory("studentPhoneNo"));
        tableStudent.setItems(list);
        tableStudent.getSelectionModel()
                .selectedItemProperty()
                .addListener(new ChangeListener<Students>() {
                    @Override
                    public void changed(ObservableValue<? extends Students> observable, Students oldValue, Students newValue) {
                        studentEntity = tableStudent.getSelectionModel().getSelectedItem();
                        viewDetail();
                    }
                });
    }
    private void loadList() {
        List<Students> students = controller.getAll(Students.class);
        list.addAll(students);
    }
    private void viewDetail() {
        if (studentEntity != null) {
            txtName.setText(studentEntity.getName());
            txtStudentPhoneNumber.setText(studentEntity.getStudentPhoneNo());
        }
    }
   public Boolean isValidate(){
        if (txtGuardianPhoneNumber.getText().isEmpty() || txtStudentPhoneNumber.getText().isEmpty()
                || txtName.getText().isEmpty()
                || txtAddress.getText().isEmpty() || txtGuardianName.getText().isEmpty()) {
            txtValidationText.setText("Invalid Credential");
            return false;
        }else
          return true;
   }
        }

