/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.studentassignmentmanagement.utility.services;

 
import com.controllers.IController;
import com.controllers.IViewController;
import com.mycompany.studentassignmentmanagement.model.BaseEntity.BaseEntity;
import com.mycompany.studentassignmentmanagement.viewController.ViewDialog.AlertDialogClass;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author admin
 * @param <T>
 */
public class WindowService<T, K extends IController> {

        public Parent getView(K iController, String viewUrl) throws IOException {

                FXMLLoader loader = new FXMLLoader(getClass().getResource(viewUrl));
                Parent layout = loader.load();
                T clazz = (T) loader.getController();
                if (clazz instanceof IViewController) {
                        ((IViewController) clazz).setController(iController);
                }
                return layout;

        }

        public static void showDialog(Boolean isErrorMsg, String titleMsg, String contentMsg) {
                new AlertDialogClass(isErrorMsg, contentMsg, false);
        }

        public static boolean saveMessage(BaseEntity prevEntity, BaseEntity newEntity, ObservableList list) {
                if (newEntity == null) { // error if server resp is null entity
                        new AlertDialogClass(true, "Error in Saving", false);
                        return false;
                } else {
                        if (prevEntity.getId() == null) {
                                list.add(newEntity);
                        } else {
                                list.set(list.indexOf(prevEntity), newEntity);
                        }
                        prevEntity = newEntity;
                        new AlertDialogClass(false, "Save Successfully", false);
                        return true;
                }
        }

        public static boolean showConfirmationDialog(String titleMsg, String headerText, String contentMsg) {

                AlertDialogClass c = new AlertDialogClass(false, contentMsg, true);
                return c.getValue();
        }

        public static String openTextInputDialog(String headerTitle, String contentText) {
                Dialog dialog = new TextInputDialog();
                dialog.setTitle("Soj Infotech");
                dialog.setContentText(contentText);
                dialog.setHeaderText(headerTitle);
                dialog.setContentText("0");
                Optional<String> result = dialog.showAndWait();
                if (result.isPresent()) {
                        return result.get();
                }
                return null;
        }

        public static class ShortCutKey {

                public static void setBtnKeyForLayout(Parent layout) {
                        Pane pane = null;
                        if (layout instanceof AnchorPane
                                || layout instanceof BorderPane
                                || layout instanceof VBox
                                || layout instanceof HBox) {

                                pane = (Pane) layout;
                        }
                        if (pane == null) {
                                return;
                        }
                        pane.getChildren().stream()
                                .forEach(nod -> {
                                        if (nod instanceof Button) {
                                                addEventhandlerOnBtn(KeyCode.ENTER, nod);
                                        }
                                        if (nod instanceof Pane) {

                                        }

                                });
                }

                public static void saveShortKey(Button btn) {
                        Scene scene = btn.getScene();
                        if (scene == null) {
                                throw new NullPointerException();
                        }
                        scene.getAccelerators().put(new KeyCodeCombination(KeyCode.S, KeyCombination.ALT_ANY),
                                (Runnable) () -> {
                                        btn.fire();
                                });
                }

                public static void addEventhandlerOnBtn(KeyCode keyCode, Button btn) {
                        btn.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
                                if (ev.getCode() == keyCode) {
                                        if (btn instanceof Button) {
                                                ((Button) btn).fire();
                                        }
                                        ev.consume();
                                }
                        });
                }

                public static void addEventhandlerOnBtn(KeyCode keyCode, Node... nodeList) {
                        for (Node btn : nodeList) {
                                btn.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
                                        if (ev.getCode() == keyCode) {
                                                if (btn instanceof Button) {
                                                        ((Button) btn).fire();
                                                }
                                                ev.consume();
                                        }
                                });
                        }
                }
        }

        public static class ViewState {

                private static Map<String, List> listOfView = new HashMap();

                private static Stack viewStack = new Stack();

                public static void addViewStack(Parent parent) {
                        if (!viewStack.contains(parent)) {
                                viewStack.add(parent);
                        }
                }

                public static Parent getLastView() {

                        if (viewStack.isEmpty()) {
                                return null;
                        }
                        if (viewStack.size() == 1) {
                                return (Parent) viewStack.get(0);
                        }
                        return (Parent) viewStack.pop();

                }

        }

}
