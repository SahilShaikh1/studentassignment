/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.studentassignmentmanagement.utility;

/**
 *
 * @author Tabassum Naaz
 */
public class Constants {
    
    public static final String STUDENTS="student";
    
    public static final String ASSIGNMENT="assignment";
    
    public static final String STUDENTASSIGNMENT="studentAssignment";
    
    public static final String USERMANAGEMENT="userManagement";
    
    
}
