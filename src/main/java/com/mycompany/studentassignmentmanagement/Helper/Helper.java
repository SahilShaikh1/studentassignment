/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.studentassignmentmanagement.Helper;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;

/**
 *
 * @author Sahil
 */
public class Helper {
    public static Date convertToDate(String dateString) {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
            return sdf.parse(dateString);
        }catch (Exception ex){
            return null;
        }
    
}
    public static Date  getDatefromLocalDate(LocalDate localDate){
        Date date = java.sql.Date.valueOf(localDate);
        return date;
    }
    
    public static void numberValidation(final TextField txt){
        txt.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                try{
                    Integer num = Integer.parseInt(newValue);
                }catch(Exception e){
                    txt.setText(txt.getText().length()==0? "": oldValue);
                }

            }
        });
    }
}
