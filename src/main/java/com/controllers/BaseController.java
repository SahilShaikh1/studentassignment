/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controllers;

import dataservice.Dao;
import dataservice.IDao;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author user
 */
public class BaseController<T> {
    
        public IDao dao;

        public BaseController() {
                dao = new Dao();
        }

        public T save(T entity) {
                try {
//                        setCreatedAndModifiedTimeStamp(entity);
                        return dao.saveOrUpdate(entity);
                } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        return entity;
                }
        }

        public T softDelete(T entity) {
                try {
                        return dao.saveOrUpdate(entity);
                } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                        return entity;
                }
        }

        public T persist(T entity) {
                try {

                        dao.persist(entity);
                } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                return entity;

        }

        
        public List<T> getAll(Class<T> type) {
                try {
                        return dao.getAll(type);
                } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                return null;

        }

        
        public T getById(Class<T> type, long id) {
                try {
                        dao.get(type, id);
                } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                return null;
        }

        public List<T> getByQuery(Class<T> type, String query) {
                try {
                        return dao.getByQuery(type, query);
                } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                return null;
        }

        
        public boolean isValidateEntity(T entity, List<T> entityList) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        
        public Object getByQuery(String query) {
                return dao.getByQuery(query);
        }

//        private void setCreatedAndModifiedTimeStamp(T entity) {
//                if (entity instanceof BaseEntity) {
//                        ((BaseEntity) entity).setIsActive(Boolean.TRUE);
//                        if (((BaseEntity) entity).getId() == null) {
//                                ((BaseEntity) entity).setCreatedDate(new Date());
//                                ((BaseEntity) entity).setCreatedUser(Applications.getInstance().getUser().getName());
//                        } else {
//                                ((BaseEntity) entity).setModifiedDate(new Date());
//                                ((BaseEntity) entity).setModifiedby(Applications.getInstance().getUser().getName());
//                        }
//                }
//
//        }

        
        public Object getByQueryWithDistinctRoot(String query) {
                return dao.getByQuery(query);
        }
    
}
