/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package  com.controllers;
import javafx.fxml.Initializable;

/**
 *
 * @author admin
 */
public interface IViewController<T> extends Initializable { 
    void setController(IController controller); 
}
